# Temperature forecasting with Amazon Forecast
## Introduction
![Amazon Forecast](images/forecast.png)

**Amazon Forecast** is a fully managed service that uses machine learning to deliver highly accurate forecasts.<br>
Companies today use everything from simple spreadsheets to complex financial planning software to attempt to accurately forecast future business outcomes such as product demand, resource needs, or financial performance. These tools build forecasts by looking at a historical series of data, which is called time series data. This approach can struggle to produce accurate forecasts for large sets of data that have irregular trends. Also, it fails to easily combine data series that change over time (such as price, discounts, web traffic, and number of employees) with relevant independent variables like product features and store locations.

**Amazon Forecast** requires no machine learning experience to get started.<br> You only need to provide historical data, plus any additional data that you believe may impact your forecasts. Once you provide your data, Amazon Forecast will automatically examine it, identify what is meaningful, and produce a forecasting model capable of making predictions that are up to 50% more accurate than looking at time series data alone.

**Amazon Forecast** is a fully managed service, so there are no servers to provision, and no machine learning models to build, train, or deploy. <br>You pay only for what you use, and there are no minimum fees and no upfront commitments.


*You can use Amazon Forecast to generate predictions on time-series data to estimate:*
*  Operational metrics, such as web traffic to servers, AWS usage, or IoT sensor metrics.
*  Business metrics, such as sales, profits, and expenses.
*  Resource requirements, such as the quantity of energy or bandwidth needed to meet a specific demand.
*  The amount of raw goods, services, or other inputs needed by a manufacturing process.
*  Retail demand considering the impact of price discounts, marketing promotions, and other campaigns.



## Scenario
![Diagram](images/labdiagramv2.jpg)<br>
In this lab we uses Amazon S3 , Amazon Athena and Amazon Forecast to generate predictions to estimate the temperature.



## Prerequisite
>Make sure the region is US East (N. Virginia), which its short name is us-east-1.

>Make sure your account can signing up to Amazon Forecast for preview access.


>Download the [sample dataset](startting_forecast.zip) and extract it.<br>
>>Or you can find other dataset at those site : [UCI ML Repository](https://archive.ics.uci.edu/ml/datasets.php) / [Kaggle](https://www.kaggle.com/datasets)<br>
Highly recommend that download data-set which data-type is **Time-Series** and **.csv** file.

## Organise the dataset
###  Create S3 bucket
-   On the Service menu,select [S3](https://s3.console.aws.amazon.com/s3/home?region=us-east-1).

-   Select **Create bucket**.

-   Type an **unique name** for your S3 bucket.

-   Select **Create** on the down-left of the create console.

-   Search for your bucket name and click it on the name to enter the bucket.

-   Select **UPLOAD**, select Add files,choose the **csv** file that we downloaded in the Prerequisites.


### Import and Convert the datafile at Athena

-   On the Service menu,select [Athena](https://console.aws.amazon.com/athena/home?region=us-east-1#).

-   To import the datafile from S3,type below standard sql on the Query Editor tab.<br>

```sql
CREATE EXTERNAL TABLE IF NOT EXISTS default.<your_table_name> (
  `item_id` string,
  `Formatted_Date` string,
  `Summary` string,
  `Precip_Type` string,
  `Temperature` float,
  `Apparent_Temperature` float,
  `Humidity` float,
  `Wind_Speed` float,
  `Wind_Bearing` float,
  `Visibility` float,
  `Loud_Cover` float,
  `Pressure` float,
  `Daily_Summary` string
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
  'escapeChar'='\\', 
  'quoteChar'='\"', 
  'serialization.format' = ',',
  'field.delim' = ','
) LOCATION 's3://<your_bucket_name>/'
TBLPROPERTIES (
  'has_encrypted_data'='false',
  'skip.header.line.count'='1'
);

```
-   Click **Run Query**.

![Import table to athena](images/import-to-athena.png)

### Transform the string formart to timestamp

-   After import the datafile from S3, type below  standard sql on the Query Editor tab to sort out the dataset.<br>

~~~sql
select item_id, CAST(formatted_date AS timestamp) timestamp, temperature as target_vaule
from <your-table-name> ;
~~~

-   Click **Run Query**.

![Transform the formart](images/convert1.png)

-   Create tables from query results, click **create table from query**
   
![Fill in](images/convert2.png)

-   Enter information below and Next.
    -   Table name : `Your_table_name`
    -   Output data format : `CSV`

> If you didn't fill in the output location, the file will save at default location automatically.
>> Default location : Amazon S3://aws-athena-query-results-242196433183-`region`/`query name or unsaved`/`year`/`month`/`date`/

-   **Click Next** -> **Click Create** and wait for two minutes.

-   Now,you will see the new csv file inside default location,which locate at S3 bucket.

![S3 bucket](images/downloadcsv.png)

-   Download the csv file , and open it with notepad.

-   Remove the header which is the attributes name and save it.

![modify](images/deletecsv.png)

-   Create the new floder within your S3 bucket, and upload the modified file.

![upload](images/uploadtos3.png)

-   Copy the file path and paste it at notepad, we will use it later.

## Deploy your data set to the forecast
### Create dataset group

-   On th Service menu,Select amazon [Forecast](https://us-east-1.console.aws.amazon.com/forecast/home?region=us-east-1#landing).

-   Select Create dataset group.

![](images/create_group1.png)

-   Fill in those attributes:
    -   Dataset group name : `my_forecast_dataset_group`
    -   Forecasting domain : `Custom`

![](images/creategp2.png)

-   Select Next.


### Create target time series dataset

-   Type `my_forecast_dataset` for Dataset name.

-   Set **Your data entries have a time interval of** 1 `hour`,and leave other settings as default.

![](images/createdataset1.png)

-   Click **Next**

### Import target time series data

-   Type `my_forecast_dataset_import` for Dataset import name.

-   Set **IAM ROLE** to `Create a new role`.

-   [x] Specify S3 bucket, and type the bucket name where you stored the dataset,select Create role.

-   Input the **path to your S3 bucket or a folder in your bucket** that contains your data to Data location.
> such as `s3://<your_bucket>/<your_folder_name>/`.

-   Your page should look like this:

![](images/createdataset2.png)

-   Select **Start Import**, this might take 5~10 minutes.

### Train a predictor

-   After the **Target time series data** becomes **Active**, select **Start** of the Predictor training.

-   Fill in those attributes :
    -   Predictor name : `my_forecast_predictor`
    -   Forecast horizon : `50`
    -   Forecast frequency : `hour`
    -   Algorithm selection : Select `NTPS`<br>
**And leave other settings as default.**

![images](images/train.png)

-   Select **Train Prediction**, this might take 10-15 minutes.


> If you choose AutoML, it will be take more times. 

### Generate forecasts

-   After the **Predictor training** becomes **Active**, select **Start** of the **Forecast generation**.

-   Choose `my_forecast_predictor` for **Predictor**, and choose the only version of the Predictor version.

-   Select **Create a froecast**, this might take 10~20 minutes.

![create a forecast](images/createforecast.png)

### Lookup forecast

-   After the Forecast generation ends, you will see two options:
    -   **Lookup forecast** and **Create a forecast**,select **Lookup forecast**.

![lookup forecast](images/afterpredictor.png)

-   Fill in those attributes:
    -   item id : `HAKATA`
    -   Predictor : `my_forecast_predictor`
    -   Start date: `2016/12/31 23:00:00`
    -   End date: `2017/01/03 19:00:00`

![lookup forecast](images/lookup3.png)

`The date range is corresponds to the Forecast horizon that you specified in Train a Predictor.`

-   Select **Get Forecast**.

-   You will see a chart like this, which presents a forecast:

![Lookup img](images/lookgenera.png)

> P90 : The lowest figure. It means that 90% of the calculated estimates will be equal or exceed P90 estimate.<br>
P50 : This is the median.<br>
P10 : The highest figure, it means that 10% of the calculated estimates will be equal or exceed P10 estimate.

## Conclusion
Congratulations!!
Now, you have learned how to predict a set of data with Amazon Forecast. <br>
Although it can help you generate a chart of prediction, but it is only 50 % correct.

## Cleanup
-   S3 bucket
-   Forecasts
-   Predictors
-   Datasets
-   Tables at Athena